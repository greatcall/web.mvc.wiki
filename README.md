# Web.Mvc
Custom MVC components such as model binders, value providers, view engines, and extension methods

### How do I install it?
###### Action Filters
`install-package GreatCall.Web.Mvc.ActionFilters`
###### Drop Down Lists
`install-package GreatCall.Web.Mvc.DropDownLists`
###### Extensions
`install-package GreatCall.Web.Mvc.Extensions`
###### Model Binders
`install-package GreatCall.Web.Mvc.ModelBinders`
###### Value Providers
`install-package GreatCall.Web.Mvc.ValueProviders`
###### View Engines
`install-package GreatCall.Web.Mvc.ViewEngines`

### How do I use it?
View the [wiki](https://bitbucket.org/greatcall/web.mvc.wiki/wiki).
